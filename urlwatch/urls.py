"""urlwatch URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from pricewatch.views import (
    index_page,
    add_url,
    refresh_entry,
    delete_entry,
    refresh_all,
)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", index_page, name="index"),
    path("add-url/", add_url, name="add-url"),
    path("refresh-entry/<int:serial>", refresh_entry, name="refresh-entry"),
    path("delete-entry/<int:serial>", delete_entry, name="delete-entry"),
    path("refresh-all/", refresh_all, name="refresh-all"),
    path("api-test/", include("pricewatch.urls")),
]
