from rest_framework import serializers
from .models import UrlWatchEntry
import re


class ItemListSerializer(serializers.ListSerializer):
    def to_representation(self, data):

        query_params = self.context.get("request").query_params
        if "pricele" in query_params:
            return super().to_representation(self.filter_less_than_price(data, "le"))
        elif "pricegr" in query_params:
            return super().to_representation(self.filter_less_than_price(data, "gr"))
        if "sort" in query_params:
            data = list(data)
            data.sort(
                key=lambda x: x.current_price,
                reverse=(True if query_params["sort"] == "asc" else False),
            )
            return super().to_representation(data)
        else:
            return super().to_representation(data)

    def filter_less_than_price(self, data, comparator):
        price_threshold = None
        try:
            price_threshold = int(
                self.context.get("request").query_params["price" + comparator]
            )
        except ValueError:
            print("Error parsing query param price" + comparator)
            return data
        to_exclude = []
        for item in data:
            item_price = re.findall(r"\d+", item.current_price)[0]
            if comparator == "le":
                if int(item_price) > price_threshold:
                    to_exclude.append(item.current_price)
            elif comparator == "gr":
                if int(item_price) < price_threshold:
                    to_exclude.append(item.current_price)
        data = data.exclude(current_price__in=to_exclude)
        return data


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        list_serializer_class = ItemListSerializer
        model = UrlWatchEntry
        fields = ("title", "current_price", "url", "last_refreshed", "serial_number")
