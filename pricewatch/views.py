import requests
import datetime
import os
import random
from django.db.models.fields import DateField

from django.shortcuts import render, redirect
from urllib.parse import urlparse
from .models import UrlWatchEntry
from bs4 import BeautifulSoup
from .serializers import ItemSerializer
from rest_framework import viewsets

# Create your views here.


class UrlViewSet(viewsets.ModelViewSet):
    queryset = UrlWatchEntry.objects.all().order_by("serial_number")
    serializer_class = ItemSerializer
    permission_classes = []


def index_page(request):
    entries = UrlWatchEntry.objects.all()
    return render(request, "index.html", {"all_entries": entries})


def add_url(request):
    # Add a URL from form to db.
    if request.method == "POST":
        url = request.POST.get("URL")
        url_validation = urlparse(url)
        if url_validation[0] == "":
            # invalid url
            return redirect("index")
        else:
            # Valid URL, make entry.
            _addUrlAsEntry(url)
    return redirect("index")


def refresh_entry(request, serial):
    # TODO: Refresh the entry associated with the serial number.
    entry_to_refresh = UrlWatchEntry.objects.get(serial_number=serial)
    _modify_entry(entry_to_refresh)
    entry_to_refresh.save()
    return redirect("index")


def delete_entry(request, serial):
    # TODO: Delete associated entry with the serial number and update the other ones.
    entry_to_delete = UrlWatchEntry.objects.filter(serial_number=serial)
    entry_to_delete.delete()
    for item in UrlWatchEntry.objects.all():
        if serial < item.serial_number:
            item.serial_number = item.serial_number - 1
            item.save()
    return redirect("index")


def refresh_all(request):
    entries = UrlWatchEntry.objects.all()
    header_list = _read_url_headers()
    for entry in entries:
        _modify_entry(entry, header_list=header_list)
        entry.save()
    return redirect("index")


# End of Views.


# Helper functions start.


def _read_url_headers():
    __location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__))
    )
    header = []
    f = open(os.path.join(__location__, "urlheaders.txt"), "r")
    for line in f:
        header.append(line[:-1])
    return header


def _addUrlAsEntry(url):
    if UrlWatchEntry.objects.filter(url=url).exists():
        return redirect("index")
    header_list = _read_url_headers()
    HEADERS = {
        "User-Agent": header_list[random.randint(0, len(header_list))],
        "Accept-Language": "en-US, en;q=0.5",
    }

    req = requests.get(url, headers=HEADERS)
    # If the header fails, retry with another random header.
    while req.status_code != 200:
        HEADERS = {
            "User-Agent": header_list[random.randint(0, len(header_list))],
            "Accept-Language": "en-US, en;q=0.5",
        }
        req = requests.get(url, headers=HEADERS)

    print("The status code is " + str(req.status_code))
    soup = BeautifulSoup(req.content, "lxml")
    item_title = _find_title(soup)
    item_price = _find_price(soup)
    if item_price == "NA":
        item_price = _find_alt_price(soup)
    entry = UrlWatchEntry(title=item_title, current_price=item_price, url=url)
    entry.save()


def _find_alt_price(soup):
    price_string = ""
    try:
        price_value = (
            soup.find("div", attrs={"id": "olp_feature_div"})
            .find("span", attrs={"a-color-price"})
            .string
        )
        price_string = price_value.strip().replace(",", "")
    except AttributeError:
        price_string = "NA"
    return price_string


def _find_title(soup):
    title_string = ""
    try:
        title = soup.find("span", attrs={"id": "productTitle"})
        title_value = title.string
        title_string = title_value.strip().replace(",", "")
    except AttributeError:
        title_string = "NA"
    return title_string


def _find_price(soup):
    price_string = ""
    try:
        price = soup.find("span", attrs={"id": "priceblock_ourprice"})
        price_value = price.string
        price_string = price_value.strip().replace(",", "")
    except AttributeError:
        price_string = "NA"
    return price_string


def _modify_entry(entry, header_list=None):
    if header_list == None:
        header_list = _read_url_headers()
    HEADERS = {
        "User-Agent": header_list[random.randint(0, len(header_list) - 1)],
        "Accept-Language": "en-US, en;q=0.5",
    }
    url = entry.url
    req = requests.get(url, headers=HEADERS)
    soup = BeautifulSoup(req.content, "lxml")
    # check new price.
    new_price_string = _find_price(soup)
    entry.assign_new_price(new_price_string)
