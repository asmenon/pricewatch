from django.test import TestCase, testcases
from .models import UrlWatchEntry

# Create your tests here.


class PriceCompareTest(TestCase):

    def setUp(self) -> None:
        test_entry = UrlWatchEntry.objects.create(serial_number=0, title='Test title', url='https://www.testurl.com',
                                                  current_price='$ 250.45', last_known_price='$ 300', last_best_price='$ 300.95', percent_change=0.0)

    def test_renew_best_price(self):
        test_entry = UrlWatchEntry.objects.get(serial_number=0)
        test_entry.renew_best_price()
        self.assertEqual(test_entry.last_best_price, '$ 250.45')

    def test_eval_change(self):
        test_entry = UrlWatchEntry.objects.get(serial_number=0)
        test_entry.eval_change()
        print(test_entry.percent_change)
        self.assertEqual(test_entry.last_known_price, '$ 250.45')
        self.assertTrue(test_entry.percent_change != 0.0)
