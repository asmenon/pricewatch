from datetime import date, datetime, time
from django.db import models
import os
import random

# Create your models here.


class UrlWatchEntry(models.Model):
    serial_number = models.IntegerField()
    title = models.CharField(max_length=720)
    url = models.URLField(unique=True)
    current_price = models.CharField(max_length=40)
    last_known_price = models.CharField(max_length=40, default="")
    last_best_price = models.CharField(max_length=40, default="")
    percent_change = models.FloatField(default=0.0)
    last_refreshed = models.DateTimeField(default=datetime.now)

    def save(self, *args, **kwargs):
        if self.serial_number is None:
            self.serial_number = UrlWatchEntry.objects.all().count()
        self.refresh_date()
        self.eval_change()
        self.renew_best_price()
        super().save(*args, **kwargs)

    def renew_best_price(self):
        # Parse last_best_price and current_price into integers and compare.
        lbp = self.__get_float(self.last_best_price)
        cp = self.__get_float(self.current_price)
        if lbp == -1 and cp != -1:
            self.last_best_price = self.current_price
        elif lbp != -1 and cp != -1 and lbp > cp:
            self.last_best_price = self.current_price

    def eval_change(self):
        # Pre: last_known_price not equal to current_price.
        # parse last_known_price and current_price as floats
        lkp = self.__get_float(self.last_known_price)
        cp = self.__get_float(self.current_price)
        if lkp == -1 and cp != -1:
            self.last_known_price = self.current_price
        if lkp != -1 and cp != -1 and lkp != cp:
            self.percent_change = 100 * (cp - lkp) / cp
        round(self.percent_change, 2)
        self.last_known_price = self.current_price

    def assign_new_price(self, new_price_str):
        # Slice the number from the symbol.
        self.current_price = new_price_str

    def __get_float(self, value_str):
        value = 0.0
        try:
            numerics = ""
            for char in value_str:
                if char.isdigit() or char == ".":
                    numerics = numerics + char
            value = float(numerics)
        except ValueError:
            value = -1.0
        return value

    def refresh_date(self):
        self.last_refreshed = datetime.now()

    def get_date_formatted(self):
        return self.last_refreshed.date()
